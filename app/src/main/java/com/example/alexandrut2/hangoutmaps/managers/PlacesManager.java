package com.example.alexandrut2.hangoutmaps.managers;

import android.text.TextUtils;
import android.util.Log;

import com.example.alexandrut2.hangoutmaps.mvp.model.FriendInviteModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.PreferencesModel;
import com.example.alexandrut2.hangoutmaps.placesapi.PlacesService;
import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.MapPlace;
import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.Result;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alexandrut2 on 4/10/17.
 */

public class PlacesManager {

    private static PlacesManager instance;

    private PlacesService service;

    private SuggestionListener suggestionListener;


    public interface SuggestionListener{
        void onSuggestionsReady(List<Result> places, List<FriendInviteModel> ids);
        void onNoSuggestions();
    }


    private PlacesManager(){
        String url = "https://maps.googleapis.com/maps/api/place/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(PlacesService.class);
    }


    public static PlacesManager getInstance(){
        if(instance == null){
            instance = new PlacesManager();
        }
        return instance;
    }

    ////////// PUBLIC METHODS //////////

    public void suggestPlaces(List<LocationModel> locations, List<PreferencesModel> preferenceModels, List<FriendInviteModel> inviteModels, int distance){
        double lat = 0.0, lon = 0.0;
        for (LocationModel l: locations) {
            lat += l.getLat();
            lon += l.getLon();
        }
        List<String> preferences = new ArrayList<>();
        for (PreferencesModel pm: preferenceModels) {
            preferences.addAll(pm.getPreferences());
        }
        Log.e("AlexT","pref List="+preferences);
        String location = lat/(double)locations.size() +","+lon/(double)locations.size();
        PreferencesManager.CategoryPreferencePair categoryPreferencePair = PreferencesManager.getPreferencesWithTreshold(preferences);
        if(categoryPreferencePair.getPreference().isEmpty()){
            categoryPreferencePair = PreferencesManager.getPreference(preferences);
        }
        String preferencesList = "";
        for (String s:categoryPreferencePair.getPreference()){
            preferencesList += s+",";
        }
        preferencesList = preferencesList.substring(0, preferencesList.length()-1);
        String category = categoryPreferencePair.getCategory();
        Log.e("AlexT","type = "+ category +", keyword="+preferencesList+" location="+location+" dist="+distance);
        Call<MapPlace> call;
        if(TextUtils.isEmpty(category)) {
            Log.e("AlexT","getSuggestionsWithTreshold");
            call = service.getSuggestionPlaces(preferencesList, location, distance);
        } else {
            Log.e("AlexT","getSuggestions");
            call = service.getSuggestionPlaces(category, preferencesList, location, distance);
        }

        call.enqueue(new Callback<MapPlace>() {
            @Override
            public void onResponse(Call<MapPlace> call, Response<MapPlace> response) {
                if(response.isSuccessful() && response.body().getResults() != null && response.body().getResults().size()>0){
                    Log.e("AlexT","response "+response.body().getResults());
                    List<Result> results = response.body().getResults();
                    sendResults(results, inviteModels);
                } else {
                    if(suggestionListener != null){
                        suggestionListener.onNoSuggestions();
                    }
                    Log.e("AlexT","response is not successfull "+ response.toString());
                }
            }

            @Override
            public void onFailure(Call<MapPlace> call, Throwable t) {
                Log.e("AlexT"," places call failed");
            }
        });
    }

    ////// PRIVATE METHODS ////////
    private void sendResults(List<Result> results, List<FriendInviteModel> inviteModels){
        Log.e("AlexT","suggestions ready");
        if(suggestionListener != null){
            suggestionListener.onSuggestionsReady(results, inviteModels);
        }
    }

    public void setListener(SuggestionListener listener) {
        this.suggestionListener = listener;
    }
}
