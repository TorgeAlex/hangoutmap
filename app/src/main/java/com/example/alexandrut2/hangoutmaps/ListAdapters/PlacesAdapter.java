package com.example.alexandrut2.hangoutmaps.ListAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.Result;
import com.example.alexandrut2.hangoutmaps.utils.ApplicationClass;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by alexandrut2 on 4/22/17.
 */

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.PlaceViewHolder> {

    ArrayList<Result> results = new ArrayList<>();

    private PlaceClickedListener listener;

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PlaceViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) ApplicationClass.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.place_listview_item, parent, false);
        holder = new PlaceViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        Result result = results.get(position);
        Places.GeoDataApi.getPlacePhotos(GoogleServicesManager.getInstance().getGoogleApiClient(), result.getId())
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {
                    @Override
                    public void onResult(@NonNull PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }
                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getScaledPhoto(GoogleServicesManager.getInstance().getGoogleApiClient(), 70,
                                            70)
                                    .setResultCallback(new ResultCallback<PlacePhotoResult>() {
                                        @Override
                                        public void onResult(PlacePhotoResult placePhotoResult) {
                                            if (!placePhotoResult.getStatus().isSuccess()) {
                                                Log.e("AlexT"," photo not succ");
                                                return;
                                            }
                                            Log.e("AlexT"," photo succ");
                                            holder.image.setImageBitmap(placePhotoResult.getBitmap());
                                        };
                                    });
                        }
                        Log.e("AlexT"," bffer == 0");
                        photoMetadataBuffer.release();
                    }
                });
        if (result.getRating() != null) {
            holder.rating.setRating(Float.valueOf(result.getRating().toString()));
        }
        holder.name.setText(result.getName());
        holder.address.setText(result.getVicinity());
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    public void setResults(ArrayList<Result> results) {
        this.results = results;
        this.notifyDataSetChanged();
    }


////////     VIEW HOLDER    ////////


    class PlaceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_place_name)
        TextView name;

        @BindView(R.id.item_place_address)
        TextView address;

        @BindView(R.id.item_place_rating)
        RatingBar rating;

        @BindView(R.id.place_image)
        ImageView image;

        private View view;

        public PlaceViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            view = v;
        }

        @OnClick(R.id.place_holder_layout)
        public void addFriendToList() {
            int pos = getAdapterPosition();
            if (listener != null) {
                listener.placeClicked(results.get(pos));
            }
        }
    }

    public void setListener(PlaceClickedListener listener) {
        this.listener = listener;
    }

    public interface PlaceClickedListener {
        void placeClicked(Result result);
    }
}
