package com.example.alexandrut2.hangoutmaps.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by alexandrut2 on 3/11/17.
 */

public class ApplicationClass extends Application{

    private static ApplicationClass instance;

    private static Context context;


    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

        context = getApplicationContext();
    }


    public static ApplicationClass getInstance() {
        return instance;
    }

    public static Context getContext() {
        return context;
    }
}
