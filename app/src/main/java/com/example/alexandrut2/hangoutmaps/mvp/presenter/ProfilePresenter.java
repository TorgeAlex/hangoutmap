package com.example.alexandrut2.hangoutmaps.mvp.presenter;

import android.text.TextUtils;

import com.example.alexandrut2.hangoutmaps.managers.AccountManager;
import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.example.alexandrut2.hangoutmaps.managers.PreferencesManager;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.PreferencesModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.User;
import com.example.alexandrut2.hangoutmaps.mvp.view.ProfileFragment;

import java.util.List;

/**
 * Created by alexandrut2 on 5/7/17.
 */

public class ProfilePresenter implements AccountManager.OnUserAttributesChangedListener , GoogleServicesManager.LocationModelListener{

    private ProfileFragment view;

    public ProfilePresenter(ProfileFragment view) {
        this.view = view;

        AccountManager.getInstance().setOnUserAttributesChangedListener(this);
        GoogleServicesManager.getInstance().setLocationModelListener(this);
    }

    public void updateUserName(String userName){
        if(!TextUtils.isEmpty(userName)){
            AccountManager.getInstance().updateUserName(userName);
        }
    }

    public void updatePreferences(List<Integer> selectedItems){
        final CharSequence[] items = PreferencesManager.getGategories();
        PreferencesModel preferencesModel = new PreferencesModel();
        for(Integer i : selectedItems){
            preferencesModel.addPreference(items[i].toString());
        }
        AccountManager.getInstance().updatePreferences(preferencesModel);
    }

    public CharSequence[] getPreferences(){
        return PreferencesManager.getGategories();
    }

    public User getCurrentUser(){
        return AccountManager.getInstance().getCurrentUser();
    }

    @Override
    public void onUpdateLocationModel(LocationModel locationModel) {
        view.updateLocation(locationModel);
    }

    public void updateUserImage(String encodedPicture){
        AccountManager.getInstance().updateUserImage(encodedPicture);
    }

    @Override
    public void userAttributesChanged(User user) {
        view.updateUserAttributes(user);
    }
}
