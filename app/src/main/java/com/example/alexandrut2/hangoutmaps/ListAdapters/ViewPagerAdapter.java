package com.example.alexandrut2.hangoutmaps.ListAdapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.alexandrut2.hangoutmaps.mvp.view.FriendsFragment;
import com.example.alexandrut2.hangoutmaps.mvp.view.MapViewFragment;
import com.example.alexandrut2.hangoutmaps.mvp.view.ProfileFragment;

/**
 * Created by alexandrut2 on 3/2/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter{
    private static final int FRAGMENT_NR = 3;

    private MapViewFragment mapViewFragment;
    private ProfileFragment profileFragment;
    private FriendsFragment friendsFragment;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Bundle args = new Bundle();
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                mapViewFragment =  MapViewFragment.newInstance("Ceva","ceva");
                args.putInt("page_position", 1);
                return mapViewFragment;
            case 1: // Fragment # 0 - This will show FirstFragment different title
                profileFragment = ProfileFragment.newInstance();
                args.putInt("page_position", position + 1);
                mapViewFragment.setArguments(args);
                return profileFragment;
            case 2:
                friendsFragment = FriendsFragment.newInstance("1,","dsada");
                args.putInt("page_position", position + 1);
                friendsFragment.setArguments(args);
                return  friendsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return FRAGMENT_NR;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "Map";
                break;
            case 1:
                title = "My Profile";
                break;
            case 2:
                title = "Place Pick";
                break;
            case 3:
                title = "Friends";
                break;
        }
        return title;
    }
}
