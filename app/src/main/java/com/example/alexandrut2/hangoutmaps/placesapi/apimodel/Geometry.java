package com.example.alexandrut2.hangoutmaps.placesapi.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by alexandrut2 on 3/14/17.
 */

public class Geometry {

    @SerializedName("location")
    @Expose
    private MapLocation location;

    /**
     *
     * @return
     * The location
     */
    public MapLocation getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(MapLocation location) {
        this.location = location;
    }

}
