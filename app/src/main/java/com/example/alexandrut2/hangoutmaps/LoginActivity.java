package com.example.alexandrut2.hangoutmaps;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrut2.hangoutmaps.managers.AccountManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static java.lang.System.exit;

public class LoginActivity extends AppCompatActivity implements AccountManager.OnLoginResponseListener {

    @BindView(R.id.password_et)
    TextView passwordEt;

    @BindView(R.id.email_et)
    TextView emailEt;

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login Screen");
        ButterKnife.bind(this);

    }

    @OnClick(R.id.log_in_button)
    public void login(View view){
        String password = passwordEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim().toLowerCase();

        if(TextUtils.isEmpty(email)){
            //TODO email is empty
            Toast.makeText(this,"Email is empty",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            //TODO password is empty
            Toast.makeText(this,"Password is empty",Toast.LENGTH_LONG).show();
            return;
        }
        AccountManager.getInstance().setOnLoginResponseListener(this);
        AccountManager.getInstance().login(email, password);
    }

    @Override
    public void onLogInSuccessfully() {
        //Toast.makeText(this,"User logged in successfully: "+currentUser.getName(),Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onLogInFailed(String status) {
        Toast.makeText(this,"User logged failed: "+status,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            startActivity(new Intent(this, RegisterActivity.class));
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to go back to register screen", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
