package com.example.alexandrut2.hangoutmaps.mvp.model;

/**
 * Created by alexandrut2 on 3/18/17.
 */

public class LocationModel {

    private double lat;

    private double lon;

    private String currentAddress;


    public LocationModel(){}

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }
}
