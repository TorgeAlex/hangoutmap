package com.example.alexandrut2.hangoutmaps.mvp.presenter;

import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import com.example.alexandrut2.hangoutmaps.mvp.model.FriendInviteModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.FriendsList;
import com.example.alexandrut2.hangoutmaps.mvp.model.User;
import com.example.alexandrut2.hangoutmaps.mvp.view.FriendsFragment;
import com.example.alexandrut2.hangoutmaps.managers.AccountManager;
import com.example.alexandrut2.hangoutmaps.managers.PlacesManager;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.PreferencesModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alexandrut2 on 4/1/17.
 */

public class FriendsPresenter implements AccountManager.OnFriendsChangedListener{

    private FriendsFragment view;

    List<User> friends = new ArrayList<>();

    Map<Integer, LocationModel> friendsLocations = new HashMap<>();

    Map<Integer, PreferencesModel> friendsPreferences = new HashMap<>();

    HashMap<Integer, FriendInviteModel> friendInviteModels = new HashMap<>();

    public FriendsPresenter(FriendsFragment view) {
        this.view = view;
        if (AccountManager.getInstance().getFriends() != null) {
            friends = AccountManager.getInstance().getFriends();
        } else {
            Log.e("AlexT", "create adapter size: is null");
        }
        AccountManager.getInstance().setFriendsChangedListener(this);
    }


    public void startSuggestionsSearch(List<Integer> friendsAdded, int distance) {
        List<LocationModel> locations = new ArrayList<>();
        List<PreferencesModel> preferences = new ArrayList<>();
        List<FriendInviteModel> inviteModels = new ArrayList<>();
        LocationModel lm = AccountManager.getInstance().getCurrentLocationModel();
        PreferencesModel pm = AccountManager.getInstance().getCurrentPreferencesModel();
        for (Integer key: friendsAdded) {
            locations.add(friendsLocations.get(key));
            preferences.add(friendsPreferences.get(key));
            inviteModels.add(friendInviteModels.get(key));
        }
        Log.e("AlexT"," startSuggestionsSearch ");
        Log.e("AlexT"," friendInviteModel = "+friendInviteModels.entrySet());
        if (lm != null && pm != null) {
            locations.add(lm);
            preferences.add(pm);
            PlacesManager.getInstance().suggestPlaces(locations, preferences, inviteModels, distance);
        } else {
            view.showToastMessage("You currently don't have a location");
        }
    }


    public void addFriend(String friendEmail) {
        if (TextUtils.isEmpty(friendEmail)) {
            view.showToastMessage("Friend email is empty");
            return;
        }
        AccountManager.getInstance().getUsersDatabaseReference().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot users : dataSnapshot.getChildren()) {
                        User u = users.getValue(User.class);
                        if (u.getEmail().equals(friendEmail)) {
                            Log.e("AlexT", "add friend email:" + friendEmail);
                            AccountManager.getInstance().addFriend(u);
                            break;
                        }
                    }

                    view.showToastMessage("Friend added successfully");
                    AccountManager.getInstance().getUsersDatabaseReference().removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("AlexT", "Database errot " + databaseError.getMessage());
            }
        });
    }

    public void setFriendPreference(int position) {
        User friend = friends.get(position);
        AccountManager.getInstance().getDatabaseReference().child(AccountManager.PREFERENCES_DB_PATH)
                .child(friend.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                PreferencesModel pm = dataSnapshot.getValue(PreferencesModel.class);
                if (pm == null) {
                    pm = new PreferencesModel();
                }
                Log.e("AlexT", "prefs for friend " + friend.getUid() + " = " + pm.getPreferences());
                friendsPreferences.put(position, pm);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AccountManager.getInstance().getDatabaseReference().child(AccountManager.PREFERENCES_DB_PATH)
                        .child(friend.getUid()).removeEventListener(this);
            }
        });
    }

    public void setFriendLocations(int position, TextView address){
        User friend = friends.get(position);
        AccountManager.getInstance().getDatabaseReference().child(AccountManager.LOCATIONS_DB_PATH)
                .child(friend.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LocationModel lm = dataSnapshot.getValue(LocationModel.class);
                Log.e("AlexT","address for friend "+friend.getUid()+" = "+lm.getCurrentAddress());
                address.setText(lm.getCurrentAddress());
                friendsLocations.put(position,lm);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                AccountManager.getInstance().getDatabaseReference().child(AccountManager.LOCATIONS_DB_PATH)
                        .child(friend.getUid()).removeEventListener(this);
            }
        });
    }

    public void addNewFriendInviteModel(int position){
        User friend = friends.get(position);
        friendInviteModels.put(position, new FriendInviteModel(friend.getUid(), friend.getName()));
    }


    public int getFriendsCount(){
        return friends.size();
    }

    public String getFriendName(int position){
        return friends.get(position).getName();
    }

    @Override
    public void friendsChanged(List<User> friendsList) {
        Log.e("ALexT", "FriendsFragment: Friendschanged size=" + friendsList.size());
        friendsLocations.clear();
        friends = friendsList;
        view.friendsChanged();
    }
}