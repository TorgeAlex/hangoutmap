package com.example.alexandrut2.hangoutmaps.managers;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by alexandrut2 on 4/15/17.
 */

public class PreferencesManager {

    private static final double PREFERENCE_TRESHOLD = 0.25;

    private static String[] restaurant = {"restaurant","burger", "cheeseburger", "pizza", "mc'donalds", "kfc"};

    private static String[] coffee = {"coffee", "espresso", "americano", "caffe latte", "mochaccino"};

    private static String[] cinema = {"movie", "cinema"};

    private static String[] mall = {"shopping", "mall","shop"};

    private static HashMap<String,String[]> categories = new HashMap<>();

    private static HashMap<String, ArrayList<String>> preferencesMap = new HashMap<>();

    private static HashMap<String, Integer> tresholdMap = new HashMap<>();


    private PreferencesManager(){}

    private static void init(){
        categories.clear();
        categories.put("restaurant",restaurant);
        categories.put("coffee", coffee);
        categories.put("cinema",cinema);
        categories.put("mall",mall);
        preferencesMap.clear();
        tresholdMap.clear();
    }

    /**
     * Returns a CategoryPreferencePair with the most voted preference
     * and its corresponding category
     * @param preferences
     * @return
     */
    static CategoryPreferencePair getPreference(List<String> preferences){
        init();
        for (String s:preferences) {
            putInMap(s);
        }
        MapEntry maxMapEntry = getPredominantCategory();
        String key = maxMapEntry.getKey();
        String preference = getPredominantPreference(maxMapEntry.getPreferences());

        return new CategoryPreferencePair(key,preference);
    }


    /**
     * Returns a CategoryPreferencePair with the most voted preferences
     * and without category
     * @param preferences
     * @return
     */
    static CategoryPreferencePair getPreferencesWithTreshold(List<String> preferences){
        init();
        double treshold = preferences.size() * PREFERENCE_TRESHOLD;
        Log.e("AlexT"," treshold="+treshold);
        List<String> chosenPreferences = new ArrayList<>();

        for (String s:preferences) {
            if(tresholdMap.containsKey(s)){
                int currentValue = tresholdMap.get(s);
                currentValue++;
                tresholdMap.replace(s,currentValue);
            } else {
                tresholdMap.put(s,1);
            }
        }

        for(Map.Entry<String, Integer> entry: tresholdMap.entrySet()){
            Log.e("AlexT"," key="+entry.getKey()+" value="+entry.getValue());
            if(entry.getValue() >= treshold){
                chosenPreferences.add(entry.getKey());
            }
        }

        Log.e("AlexT","getPreferencesWithTreshold = "+chosenPreferences);

        return new CategoryPreferencePair("",chosenPreferences);
    }


    /**
     * Gets the predominant category
     * @return
     */

    private static MapEntry getPredominantCategory(){
        int max = 0;
        MapEntry me = null;
        for (Map.Entry<String, ArrayList<String>> entry : preferencesMap.entrySet()){
            if(max < entry.getValue().size()){
                me = new MapEntry(entry.getKey(), entry.getValue());
                max = entry.getValue().size();
            }
        }
        return me;
    }

    /**
     * Gets the predominant preference (from one category)
     * @param preferences
     * @return
     */
    private static String getPredominantPreference(List<String> preferences){
        if(preferences.size() == 1){
            return preferences.get(0);
        }

        preferences.sort(new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });
        preferences.add("null comparator");
        String maxPred = preferences.get(0);
        int maxOc = 0;
        int oc = 0;
        for(int i = 0; i<preferences.size() - 1;i++){
            if(preferences.get(i).equals(preferences.get(i+1))){
                oc ++;
            } else {
                if(oc >= maxOc){
                    maxPred = preferences.get(i);
                    maxOc = oc;
                }
            }
        }
        return maxPred;
    }


    /**
     * Adds to the preference map the current preference
     * If the category exists, it adds the preference
     * @param preference
     */
    private static void putInMap(String preference){
        String key = getKey(preference);
        if(preferencesMap.containsKey(key)){
            ArrayList<String> values = preferencesMap.get(key);
            values.add(preference);
            preferencesMap.put(key,values);
        } else {
            ArrayList<String> pref = new ArrayList<>();
            pref.add(preference);
            preferencesMap.put(key,pref);
        }
    }

    private static String getKey(String preference){
        for (Map.Entry<String, String[]> entry : categories.entrySet()){
            if(isPreferenceInCategory(entry.getValue(), preference)){
                return entry.getKey();
            }
        }
        return "not found";
    }

    private static boolean isPreferenceInCategory(String[] category, String preference){
        for (String categoryItem:category) {
            if(categoryItem.contains(preference) || preference.contains(categoryItem)){
                return true;
            }
        }
        return false;
    }


    private static class MapEntry{
        private String key;

        private ArrayList<String> string;

        MapEntry(String key, ArrayList<String> string) {
            this.key = key;
            this.string = string;
        }

        String getKey() {
            return key;
        }

        List<String> getPreferences() {
            return string;
        }
    }


    public static String[] getGategories(){
        ArrayList<String> items = new ArrayList<>();
        items.addAll(Arrays.asList(restaurant));
        items.addAll(Arrays.asList(coffee));
        items.addAll(Arrays.asList(cinema));
        items.addAll(Arrays.asList(mall));
        return items.toArray(new String[0]);
    }



    public static class CategoryPreferencePair {
        private String category;
        private List<String> preferences = new LinkedList<>();

        public CategoryPreferencePair(String category, String preference){
            this.category = category;
            this.preferences.add(preference);
        }

        public CategoryPreferencePair(String category, List<String> preferences) {
            this.category = category;
            this.preferences = preferences;
        }

        public String getCategory() {
            return category;
        }

        public List<String> getPreference() {
            return preferences;
        }
    }

}
