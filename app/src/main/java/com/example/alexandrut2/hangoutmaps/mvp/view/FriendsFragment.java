package com.example.alexandrut2.hangoutmaps.mvp.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.mvp.model.VerticalSpaceItemDecoration;
import com.example.alexandrut2.hangoutmaps.mvp.presenter.FriendsPresenter;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class FriendsFragment extends Fragment {

    private static final String[] KM_STRINGS = {"0.5 km", "1 km", "2 km", "5 km", "10 km"};

    private static final int[] KM_VALUES = {500, 1000, 2000, 5000, 10000};

    private int selectedDistanceIndex = 0;

    @BindView(R.id.frineds_list)
    RecyclerView friendsList;


    @BindView(R.id.km_text)
    TextView kmText;

    @BindView(R.id.suggest_places_button)
    Button suggestButton;

    NumberPicker kmPicker;

    List<Integer> friendsAdded = new ArrayList<>();


    FriendsListAdapter adapter;

    LinearLayoutManager layoutManager;

    FriendsPresenter presenter;

    public FriendsFragment() {
    }

    /**
     * Constructor
     *
     * @param param1
     * @param param2
     * @return
     */
    public static FriendsFragment newInstance(String param1, String param2) {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /////////    FRAGMENT METHODS    /////////
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_friends, container, false);
        ButterKnife.bind(this, v);
        presenter = new FriendsPresenter(this);
        adapter = new FriendsListAdapter();
        friendsList.setAdapter(adapter);
        friendsList.addItemDecoration(new VerticalSpaceItemDecoration(5));
        layoutManager = new LinearLayoutManager(getContext());
        friendsList.setLayoutManager(layoutManager);
        handleSuggestButton();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void friendsChanged() {
        adapter.notifyDataSetChanged();
    }


    //// METHODS ///////
    public void handleSuggestButton() {
        if (friendsAdded.size() == 0) {
            suggestButton.setText("Please select friends");
            suggestButton.setEnabled(false);
        } else {
            suggestButton.setText("Suggest Place");
            suggestButton.setEnabled(true);
        }
    }


    /////// ON  CLICK    ///////
    @OnClick(R.id.add_friend_button)
    public void addFriend() {
        final Dialog addFriendDialog = new Dialog(getContext());
        addFriendDialog.setContentView(R.layout.add_friend_view);
        addFriendDialog.setTitle("Add friend");
        EditText friendEmailET = (EditText) addFriendDialog.findViewById(R.id.friends_email_et);
        Button add = (Button) addFriendDialog.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String friendEmail = friendEmailET.getText().toString().toLowerCase().trim();
                presenter.addFriend(friendEmail);
                addFriendDialog.dismiss();
            }
        });
        Button cancel = (Button) addFriendDialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriendDialog.dismiss();
            }
        });
        addFriendDialog.show();
    }

    @OnClick(R.id.suggest_places_button)
    public void startSuggestions() {
        Log.e("AlexT","startAutosuggestions");
        int distance = KM_VALUES[selectedDistanceIndex];
        presenter.startSuggestionsSearch(friendsAdded, distance);
    }


    @OnClick(R.id.km_text)
    public void selectDistance() {
        kmPicker = new NumberPicker(getActivity());
        kmPicker.setMinValue(0);
        kmPicker.setMaxValue(4);
        kmPicker.setDisplayedValues(KM_STRINGS);
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Select distance");
        builder.setView(kmPicker);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedDistanceIndex = kmPicker.getValue();
                kmText.setText(KM_STRINGS[selectedDistanceIndex]);
                Log.e("AlexT", "distance set to " + KM_STRINGS[selectedDistanceIndex]);
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    ///////////// ADAPTERS /////////////


    /**
     * FRIENDS LIST ADAPTER
     */
    public class FriendsListAdapter extends RecyclerView.Adapter<FriendHolder> {

        public FriendsListAdapter() {

        }

        @Override
        public FriendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            FriendHolder holder = null;
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.friend_list_item, parent, false);
            holder = new FriendHolder(v);
            return holder;
        }

        @Override
        public void onBindViewHolder(FriendHolder holder, int position) {

            presenter.addNewFriendInviteModel(position);

            presenter.setFriendLocations(position,holder.address);

            holder.name.setText(presenter.getFriendName(position));
            presenter.setFriendPreference(position);


            if (friendsAdded.contains(position)) {
                holder.addButton.setBackgroundResource(R.drawable.friend_added);
            } else {
                holder.addButton.setBackgroundResource(R.drawable.add_friend);
            }
        }

        @Override
        public int getItemCount() {
            return presenter.getFriendsCount();
        }
    }


    class FriendHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.friend_name)
        TextView name;

        @BindView(R.id.friend_address)
        TextView address;

        @BindView(R.id.add_button)
        Button addButton;

        private View view;

        public FriendHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            view = v;
        }

        @OnClick(R.id.add_button)
        public void addFriendToList() {
            int pos = getAdapterPosition();
            if (friendsAdded.contains(pos)) {
                Log.e("AlexT","friendsAdded before "+friendsAdded);
                friendsAdded.remove(new Integer(pos));
                Log.e("AlexT","friendsAdded after "+friendsAdded);
                addButton.setBackgroundResource(R.drawable.add_friend);
                handleSuggestButton();
            } else {
                friendsAdded.add(pos);
                addButton.setBackgroundResource(R.drawable.friend_added);
                handleSuggestButton();
            }


        }
    }

}