package com.example.alexandrut2.hangoutmaps.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by alexandrut2 on 3/18/17.
 */

public class AppUtils {

    /**
     * Encodes an image
     * @param image image to be encoded
     * @return String representing the encoded image
     */
    public static String encodeImage(Bitmap image){
        ByteArrayOutputStream bYtE = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bYtE);
        image.recycle();
        byte[] byteArray = bYtE.toByteArray();
        String encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encodedImage;
    }

    /**
     * Decodes an image
     * @param encodedImage image to be decoded
     * @return Bitmap representing the decoded image
     */
    public static Bitmap decodeImage(String encodedImage){
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    /**
     * Returns the address given geocoder and lat lon
     * @param geocoder
     * @param lat
     * @param lon
     * @return
     */
    public static String getCuttentAddress(Geocoder geocoder, double lat, double lon){
        Address address = null;
        List<Address> addresses = null;
        String fullAddress = "";
        try {
            addresses = geocoder.getFromLocation(lat,lon,1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(addresses != null && !addresses.isEmpty() && addresses.get(0)!=null){
            address = addresses.get(0);
        }
        if(address != null) {
            fullAddress = address.getAddressLine(0) + ", " + address.getLocality();
        } else {
            fullAddress = "Address not found";
        }
        return fullAddress;
    }


    public static String encode(String value) {
        return  "Mail"+Base64.encodeToString(value.getBytes(), Base64.DEFAULT);
    }

    public static String decodeString(String encodedString){
        return  (new String(Base64.decode(encodedString.getBytes(), Base64.DEFAULT))).substring(4);
    }
}
