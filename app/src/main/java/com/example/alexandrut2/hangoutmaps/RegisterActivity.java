package com.example.alexandrut2.hangoutmaps;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandrut2.hangoutmaps.managers.AccountManager;
import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements AccountManager.OnRegisterResponseListener, AccountManager.OnGoogleSignInResponseListener {

    private static final int RC_SIGN_IN = 100;

    @BindView(R.id.username_et)
    TextView userNameEt;

    @BindView(R.id.password_et)
    TextView passwordEt;

    @BindView(R.id.email_et)
    TextView emailEt;

    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Register Screen");
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
    }


    @OnClick(R.id.register_button)
    public void signIn(View view){

        progressDialog.setMessage(getResources().getString(R.string.register_loading_message));
        progressDialog.show();

        String username = userNameEt.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim().toLowerCase();

        if(TextUtils.isEmpty(username)){
            //TODO user is empty
            Toast.makeText(this,"Username is empty",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(email)){
            //TODO email is empty
            Toast.makeText(this,"Email is empty",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            //TODO password is empty
            Toast.makeText(this,"Password is empty",Toast.LENGTH_LONG).show();
            return;
        }
        AccountManager.getInstance().setOnRegisterResponseListener(this);
        AccountManager.getInstance().registerUser(username,email,password);
    }


    @OnClick(R.id.sing_in_text)
    public void goToSignIn(){
        finish();
        startActivity(new Intent(this,LoginActivity.class));
    }

    @OnClick(R.id.sign_in_google_button)
    public void googleSignIn(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(GoogleServicesManager.getInstance().getGoogleApiClient());
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(!result.isSuccess()){
                onGoogleSignInFailed(result.getStatus().getStatusMessage());
            } else {
                GoogleSignInAccount account = result.getSignInAccount();
                AccountManager.getInstance().setOnGoogleSignInResponseListener(this);
                AccountManager.getInstance().signInhWithGoogle(account);
            }
        }

    }

    @Override
    public void onGoogleSignInSuccessfully() {
        progressDialog.dismiss();
        goToMainActivity();
    }

    @Override
    public void onGoogleSignInFailed(String status) {
        progressDialog.dismiss();
        Toast.makeText(this,"Google sign in failed: "+status,Toast.LENGTH_LONG).show();
    }



    @Override
    public void onCreateUserSuccessfully() {
        progressDialog.dismiss();
        goToMainActivity();
    }

    @Override
    public void onCreateUserFailed(String status) {
        progressDialog.dismiss();
        Toast.makeText(this,"User created failed: "+status,Toast.LENGTH_LONG).show();
    }

    public void goToMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

}
