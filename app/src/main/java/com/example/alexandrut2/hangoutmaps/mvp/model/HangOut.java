package com.example.alexandrut2.hangoutmaps.mvp.model;

import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.Result;

import java.util.List;
import java.util.Map;

/**
 * Created by alexandrut2 on 5/27/17.
 */

public class HangOut {

    private String hostName;
    private Map<String, Boolean> participants;
    private List<String> participantsNames;
    private Result location;

    public HangOut(){}

    public HangOut(String hostName, Map<String, Boolean> participants, List<String> participantsNames, Result location) {
        this.hostName = hostName;
        this.participants = participants;
        this.location = location;
        this.participantsNames = participantsNames;
    }

    public boolean isInHangout(String uid){
        if(participants.containsKey(uid) && participants.get(uid)){
            return true;
        } else {
            return false;
        }
    }

    public void hangOutSeen(String uid){
        participants.put(uid, false);
    }


    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Map<String, Boolean> getParticipants() {
        return participants;
    }

    public void setParticipants(Map<String, Boolean> participants) {
        this.participants = participants;
    }

    public Result getLocation() {
        return location;
    }

    public void setLocation(Result location) {
        this.location = location;
    }

    public List<String> getParticipantsNames() {
        return participantsNames;
    }

    public void setParticipantsNames(List<String> participantsNames) {
        this.participantsNames = participantsNames;
    }
}
