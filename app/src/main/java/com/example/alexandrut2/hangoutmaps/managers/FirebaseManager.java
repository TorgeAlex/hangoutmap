package com.example.alexandrut2.hangoutmaps.managers;

/**
 * Created by alexandrut2 on 3/18/17.
 */

public class FirebaseManager {

    private static FirebaseManager instance;

    private FirebaseManager(){}


    private static FirebaseManager getInstance(){
        if(instance == null){
            instance = new FirebaseManager();
        }
        return instance;
    }

}
