package com.example.alexandrut2.hangoutmaps.managers;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.Language;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.constant.Unit;
import com.akexorcist.googledirection.model.Direction;
import com.example.alexandrut2.hangoutmaps.utils.ApplicationClass;
import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.utils.AppUtils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

/**
 * Created by alexandrut2 on 3/11/17.
 */

public class GoogleServicesManager implements GoogleApiClient.ConnectionCallbacks, LocationListener {

    private Location lastLocation;

    private static GoogleServicesManager instance;

    private LocationListener locationListener;

    private LocationModelListener locationModelListener;

    private RouteResultListener routeResultListener;

    private LocationRequest locationRequest;

    private GoogleMap googleMap;

    private boolean needUpdateListener = false;

    /**
     * GoogleApi Client
     */
    private GoogleApiClient googleApiClient;

    /**
     * Google sign in options
     */
    private GoogleSignInOptions gso;

    /**
     * Geocode
     */
    private Geocoder geocoder;


    /**
     * Constructor
     */
    private GoogleServicesManager() {

    }


    public void init(){
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(ApplicationClass.getInstance().getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(ApplicationClass.getContext())
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        googleApiClient.connect();

        geocoder = new Geocoder(ApplicationClass.getContext(), Locale.getDefault());

        createLocationRequest();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(ApplicationClass.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(ApplicationClass.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("AlexT","No Perms");
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(2000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(ApplicationClass.getInstance(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ApplicationClass.getInstance(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("AlexT", "no location perms");
            return;
        }

        if(needUpdateListener && locationListener != null){
            startLocationUpdates();
            needUpdateListener = false;
        }

        lastLocation = LocationServices.FusedLocationApi
                .getLastLocation(googleApiClient);
        if (locationListener != null && lastLocation != null) {
            locationListener.onUpdateLocation(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("AlexT", "onConnectionSuspended");
    }



    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        String address = AppUtils.getCuttentAddress(geocoder,location.getLatitude(), location.getLongitude());
        //Create location model
        LocationModel locationModel = new LocationModel();
        locationModel.setCurrentAddress(address);
        locationModel.setLat(location.getLatitude());
        locationModel.setLon(location.getLongitude());
        AccountManager.getInstance().updateUserLocation(locationModel);

        if(locationListener != null){
            locationListener.onUpdateLocation(new LatLng(location.getLatitude(),location.getLongitude()));
        }
        if(locationModelListener != null){
            locationModelListener.onUpdateLocationModel(locationModel);
        }
    }

    public void searchForRoute(LatLng dest){
        LatLng currentLatLon = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        GoogleDirection.withServerKey("AIzaSyBlZVIJmVS3Ynlke5TbFAB5sNw5rPaU-o4")
                .from(currentLatLon)
                .to(dest)
                .language(Language.ROMANIAN)
                .unit(Unit.METRIC)
                .transitMode(TransportMode.WALKING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if(routeResultListener != null){
                            routeResultListener.onRoutesReady(direction);
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        if(routeResultListener != null){
                            routeResultListener.onRoutesFailed(t.getMessage());
                        }
                    }
                });
    }

    ////////////////////////// GETTERS /////////////////////////////


    public Location getLastLocation() {
        return lastLocation;
    }

    public static GoogleServicesManager getInstance() {
        if(instance == null){
            instance = new GoogleServicesManager();
        }
        return instance;
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    ////////////////////////// SETTERS /////////////////////////////

    /**
     * setter for locationListener
     * @param locationListener
     */
    public void setLocationListener(LocationListener locationListener) {
        this.locationListener = locationListener;
        if(getGoogleApiClient().isConnected()) {
            startLocationUpdates();
        } else {
            needUpdateListener = true;
        }
    }

    public void setLocationModelListener(LocationModelListener locationModelListener) {
        this.locationModelListener = locationModelListener;
    }

    public void setRouteResultListener(RouteResultListener routeResultListener) {
        this.routeResultListener = routeResultListener;
    }

    public void setGoogleMap(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    //////////////////////// Listeners ////////////////////////

    /**
     * Google location Listener
     */
    public interface LocationListener{
        void onUpdateLocation(LatLng latLng);
    }

    /**
     * Google location model Listener
     */
    public interface LocationModelListener{
        void onUpdateLocationModel(LocationModel locationModel);
    }

    public interface  RouteResultListener{
        void onRoutesReady(Direction direction);
        void onRoutesFailed(String errorMessage);
    }

}
