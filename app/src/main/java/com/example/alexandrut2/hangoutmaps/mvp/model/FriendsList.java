package com.example.alexandrut2.hangoutmaps.mvp.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexandrut2 on 4/1/17.
 */

public class FriendsList {

    List<String> friendsIds = new LinkedList<>();

    String email = "";

    public List<String> getFriends() {
        return friendsIds;
    }

    public void setFriends(List<String> friends) {
        this.friendsIds = friends;
    }

    public void addFriend(String u){
        friendsIds.add(u);
    }

    public String getFriendId(int pos){
        return friendsIds.get(pos);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean containsFriend(User u){
        for (String userId:friendsIds) {
            if(userId.equals(u)){
                return true;
            }
        }
        return false;
    }
}
