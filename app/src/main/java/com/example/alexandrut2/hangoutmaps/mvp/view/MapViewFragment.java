package com.example.alexandrut2.hangoutmaps.mvp.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.example.alexandrut2.hangoutmaps.mvp.presenter.MapViewPresenter;
import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.Result;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MapViewFragment extends Fragment {

    @BindView(R.id.start_navigation_button)
    Button startNavigationButton;

    private MapViewPresenter presenter;
    private MapView mMapView;

    private static MapViewFragment instance;


    private String label = "";
    private double lat;
    private double lng;

    private MapViewFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MapViewFragment newInstance(String param1, String param2) {
        if(instance == null){
            instance = new MapViewFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_map_view, container, false);
        ButterKnife.bind(this, rootView);
        mMapView = (MapView) rootView.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        if(presenter == null) {
            presenter = new MapViewPresenter(this);
        }

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(presenter);
        return rootView;
    }

    @OnClick(R.id.start_navigation_button)
    public void startNavigation(){
        String format = "geo:0,0?q=" + Double.toString(lat) + "," + Double.toString(lng) + "(" + label + ")";
        Uri uri = Uri.parse(format);

        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setPackage("com.google.android.apps.maps");
        getContext().startActivity(intent);

        startNavigationButton.setVisibility(View.GONE);
        GoogleServicesManager.getInstance().getGoogleMap().clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public Context getViewContext(){
        return getContext();
    }

    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }



    public void showSuggestion(Result result, Direction direction){
        GoogleMap googleMap = GoogleServicesManager.getInstance().getGoogleMap();
        if(googleMap != null){
            LatLng ll = new LatLng(result.getGeometry().getLocation().getLat(),result.getGeometry().getLocation().getLng()) ;
            googleMap.addMarker(new MarkerOptions().position(ll).title(result.getName()).snippet(result.getVicinity()));
            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(ll).zoom(16).build();

            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            Route route = direction.getRouteList().get(0);
            Leg leg = route.getLegList().get(0);
            ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
            PolylineOptions polylineOptions = DirectionConverter.createPolyline(getContext(), directionPositionList, 5, Color.BLUE);
            googleMap.addPolyline(polylineOptions);

            label = result.getName();
            lat = ll.latitude;
            lng = ll.longitude;

            startNavigationButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
