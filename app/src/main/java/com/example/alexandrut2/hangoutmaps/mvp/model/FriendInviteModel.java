package com.example.alexandrut2.hangoutmaps.mvp.model;

/**
 * Created by alexandrut2 on 5/28/17.
 */

public class FriendInviteModel {
    String friendId;
    String friendName;

    public FriendInviteModel(){}

    public FriendInviteModel(String friendId, String friendName) {
        this.friendId = friendId;
        this.friendName = friendName;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    @Override
    public String toString() {
        return "FriendInviteModel{" +
                "friendId='" + friendId + '\'' +
                ", friendName='" + friendName + '\'' +
                '}';
    }
}
