package com.example.alexandrut2.hangoutmaps.placesapi;

import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.MapPlace;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alexandrut2 on 3/14/17.
 */

public interface PlacesService {

    @GET("nearbysearch/json?sensor=true&key=AIzaSyBlZVIJmVS3Ynlke5TbFAB5sNw5rPaU-o4&opennow=true")
    Call<MapPlace> getSuggestionPlaces(@Query("type")String type, @Query("keyword") String keyword, @Query("location") String location, @Query("radius") int radius);

    @GET("nearbysearch/json?sensor=true&key=AIzaSyBlZVIJmVS3Ynlke5TbFAB5sNw5rPaU-o4&opennow=true")
    Call<MapPlace> getSuggestionPlaces(@Query("keyword") String keyword, @Query("location") String location, @Query("radius") int radius);


    @GET("nearbysearch/json?rsensor=true&key=AIzaSyBlZVIJmVS3Ynlke5TbFAB5sNw5rPaU-o4&opennow=true")
    Call<MapPlace> getSuggestionPlaces(@Query("location") String location, @Query("radius") int radius);
}
