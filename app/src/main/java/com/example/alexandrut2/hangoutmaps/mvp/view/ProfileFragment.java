package com.example.alexandrut2.hangoutmaps.mvp.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.mvp.presenter.ProfilePresenter;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.User;
import com.example.alexandrut2.hangoutmaps.utils.AppUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProfileFragment extends Fragment {

    private static final int CAMERA_REQUEST = 1888; // field

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.current_address)
    TextView currentAddress;
    @BindView(R.id.user_image)
    ImageView userImage;

    private ProfilePresenter presenter;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @OnClick(R.id.name)
    public void changeName(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        final EditText input = new EditText(getActivity());
        input.setHint("name");
        alertDialog.setTitle("Update User Name");
        alertDialog.setMessage("Set new user name");
        alertDialog.setView(input);
        alertDialog.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String userName = input.getText().toString().trim();
                presenter.updateUserName(userName);
            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.user_image)
    public void takePicture(){
        Intent cameraIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @OnClick(R.id.preferences_button)
    public void updatePreferences(){
        // arraylist to keep the selected items
        final ArrayList<Integer> selectedItems=new ArrayList();

        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle("Select Preferences")
                .setMultiChoiceItems(presenter.getPreferences(), null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            selectedItems.add(indexSelected);
                        } else if (selectedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            selectedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        presenter.updatePreferences(selectedItems);
                        dialog.dismiss();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                        dialog.dismiss();
                    }
                }).create();
        dialog.show();
    }





    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this,view);
        presenter = new ProfilePresenter(this);

        User u = presenter.getCurrentUser();
        if(u != null){
            Log.e("AlexT","User is not null");
            name.setText(u.getName());
            if(!TextUtils.isEmpty(u.getImage())){
                Bitmap image = AppUtils.decodeImage(u.getImage());
                userImage.setImageBitmap(image);
            }
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        Log.e("AlexT","user attach");
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        Log.e("AlexT","user detach");
        super.onDetach();
    }


    public void updateUserAttributes(User user) {
        Log.e("ALexT","user:"+user.toString());
        name.setText(user.getName());
        if(!TextUtils.isEmpty(user.getImage())){
            Bitmap image = AppUtils.decodeImage(user.getImage());
            userImage.setImageBitmap(Bitmap.createScaledBitmap(image,120,120,false));
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap picture = (Bitmap) data.getExtras().get("data");//this is your bitmap image and now you can do whatever you want with this
            String encodedPicture = AppUtils.encodeImage(picture);
            presenter.updateUserImage(encodedPicture);
            //imageView.setImageBitmap(picture); //for example I put bmp in an ImageView
        }
    }



    public void updateLocation(LocationModel locationModel) {
        currentAddress.setText(locationModel.getCurrentAddress());
    }
}
