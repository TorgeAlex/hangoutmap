package com.example.alexandrut2.hangoutmaps.mvp.model;

import java.util.ArrayList;

/**
 * Created by alexandrut2 on 4/16/17.
 */

public class PreferencesModel {
    private ArrayList<String> preferences = new ArrayList<>();

    public PreferencesModel(){}

    public void addPreference(String preference){
        preferences.add(preference);
    }

    private void clearPreferences(){
        preferences.clear();
    }

    public ArrayList<String> getPreferences() {
        return preferences;
    }

    public void setPreferences(ArrayList<String> preferences) {
        this.preferences = preferences;
    }
}
