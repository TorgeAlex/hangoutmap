package com.example.alexandrut2.hangoutmaps.mvp.presenter;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.example.alexandrut2.hangoutmaps.mvp.view.MapViewFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by alexandrut2 on 6/11/17.
 */

public class MapViewPresenter implements OnMapReadyCallback, GoogleServicesManager.LocationListener {

    private MapViewFragment view;

    private boolean firstTime = true;

    public MapViewPresenter(MapViewFragment view) {
        this.view = view;
    }


    @Override
    public void onUpdateLocation(LatLng latLng) {
        GoogleMap googleMap = GoogleServicesManager.getInstance().getGoogleMap();
        if(googleMap != null && firstTime) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            firstTime = false;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(view.getViewContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(view.getViewContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            view.showToastMessage("You don't have the required permissions");
            return;
        }
        googleMap.setMyLocationEnabled(true);
        GoogleServicesManager.getInstance().setGoogleMap(googleMap);
        GoogleServicesManager.getInstance().setLocationListener(this);
    }
}
