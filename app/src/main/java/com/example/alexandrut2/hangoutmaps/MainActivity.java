package com.example.alexandrut2.hangoutmaps;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.model.Direction;
import com.example.alexandrut2.hangoutmaps.ListAdapters.PlacesAdapter;
import com.example.alexandrut2.hangoutmaps.ListAdapters.ViewPagerAdapter;
import com.example.alexandrut2.hangoutmaps.mvp.model.FriendInviteModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.HangOut;
import com.example.alexandrut2.hangoutmaps.mvp.view.MapViewFragment;
import com.example.alexandrut2.hangoutmaps.managers.AccountManager;
import com.example.alexandrut2.hangoutmaps.managers.GoogleServicesManager;
import com.example.alexandrut2.hangoutmaps.managers.PlacesManager;
import com.example.alexandrut2.hangoutmaps.placesapi.apimodel.Result;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements PlacesManager.SuggestionListener {

    ViewPagerAdapter viewPagerAdapter;
    boolean doubleBackToExitPressedOnce = false;
    ViewPager viewPager;
    private Result selectedResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setTitle(AccountManager.getInstance().getCurrentUser().getName());
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.pager_header);
        PlacesManager.getInstance().setListener(this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                FirebaseDatabase.getInstance().getReference().child("hangouts").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChildren()){
                            for(DataSnapshot ds: dataSnapshot.getChildren()){
                                HangOut hangOut = ds.getValue(HangOut.class);
                                checkForNewHangout(hangOut, ds.getKey());
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }, 2000);

    }




    public void checkForNewHangout(HangOut hangout, String key){
        if(hangout.isInHangout(AccountManager.getInstance().getCurrentFirebaseUser().getUid())){
            markHangoutAsDone(hangout, key);
            showInvitationDialog(hangout);
        }
    }


    public void showInvitationDialog(HangOut hangOut){
        final Dialog invitationDialog = new Dialog(this);
        invitationDialog.setContentView(R.layout.invite_view);
        TextView host = (TextView)invitationDialog.findViewById(R.id.host);
        TextView participants = (TextView)invitationDialog.findViewById(R.id.participants);
        TextView place = (TextView)invitationDialog.findViewById(R.id.hang_out_place);
        Button accept = (Button) invitationDialog.findViewById(R.id.accept_button);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleServicesManager.getInstance().setRouteResultListener(new GoogleServicesManager.RouteResultListener() {
                    @Override
                    public void onRoutesReady(Direction direction) {
                        ((MapViewFragment) viewPagerAdapter.getItem(0)).showSuggestion(hangOut.getLocation(), direction);
                    }

                    @Override
                    public void onRoutesFailed(String errorMessage) {

                    }
                });
                LatLng destLatLon = new LatLng(hangOut.getLocation().getGeometry().getLocation().getLat(), hangOut.getLocation().getGeometry().getLocation().getLng());
                GoogleServicesManager.getInstance().searchForRoute(destLatLon);
                invitationDialog.dismiss();
            }
        });
        Button decline = (Button) invitationDialog.findViewById(R.id.decline_button);
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invitationDialog.dismiss();
            }
        });
        host.setText("Host: "+hangOut.getHostName());
        place.setText("Place: "+hangOut.getLocation().getName());
        String participantsNames = "";
        for(String name: hangOut.getParticipantsNames()){
            participantsNames += name += ", ";
        }
        participants.setText("Participants: "+participantsNames.substring(0, participantsNames.length()-3));
        invitationDialog.show();

    }

    public void markHangoutAsDone(HangOut hangOut, String key){
        hangOut.hangOutSeen(FirebaseAuth.getInstance().getCurrentUser().getUid());
        boolean isDone = true;
        for(Map.Entry<String, Boolean> entry: hangOut.getParticipants().entrySet()){
            if(entry.getValue()){
                isDone = false;
            }
        }
        if(isDone) {
            FirebaseDatabase.getInstance().getReference().child("hangouts").child(key).setValue(null);
        } else {
            FirebaseDatabase.getInstance().getReference().child("hangouts").child(key).setValue(hangOut);
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            AccountManager.getInstance().signOut();
            viewPager = null;
            viewPagerAdapter = null;
            startActivity(new Intent(this, RegisterActivity.class));
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to Log Out", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onSuggestionsReady(List<Result> places, List<FriendInviteModel> friendInviteModels) {
        viewPager.setCurrentItem(0);
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Pick one");
        RecyclerView placesList = new RecyclerView(getApplicationContext());
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        placesList.setLayoutManager(manager);
        PlacesAdapter adapter = new PlacesAdapter();
        placesList.setAdapter(adapter);
        adapter.setResults((ArrayList<Result>) places);
        adapter.notifyDataSetChanged();
        builder.setView(placesList);
        AlertDialog dialog = builder.show();
        adapter.setListener(new PlacesAdapter.PlaceClickedListener() {
            @Override
            public void placeClicked(Result result) {
                dialog.dismiss();
                createHangOut(result, friendInviteModels);
                selectedResult = result;
                GoogleServicesManager.getInstance().setRouteResultListener(new GoogleServicesManager.RouteResultListener() {
                    @Override
                    public void onRoutesReady(Direction direction) {
                        ((MapViewFragment) viewPagerAdapter.getItem(0)).showSuggestion(selectedResult, direction);
                    }

                    @Override
                    public void onRoutesFailed(String errorMessage) {

                    }
                });
                LatLng destLatLon = new LatLng(result.getGeometry().getLocation().getLat(), result.getGeometry().getLocation().getLng());
                GoogleServicesManager.getInstance().searchForRoute(destLatLon);
            }
        });
    }

    @Override
    public void onNoSuggestions() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("No places found");
        builder.setMessage("There were no places found matching your preferences. You can increase the range or change your preferences an try again.");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }


    private void createHangOut(Result result, List<FriendInviteModel> inviteModels){
        HashMap<String, Boolean> participants = new HashMap<String, Boolean>();
        List<String> participantsNames = new ArrayList<>();
        for (FriendInviteModel inv: inviteModels) {
            participants.put(inv.getFriendId(), true);
            participantsNames.add(inv.getFriendName());
        }
        HangOut hangOut = new HangOut(FirebaseAuth.getInstance().getCurrentUser().getDisplayName(), participants, participantsNames,result);
        FirebaseDatabase.getInstance().getReference().child("hangouts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<HangOut> hangouts = new ArrayList<HangOut>();
                if(dataSnapshot.hasChildren()){
                    for(DataSnapshot ho: dataSnapshot.getChildren()){
                        hangouts.add(ho.getValue(HangOut.class));
                    }
                }
                hangouts.add(hangOut);
                FirebaseDatabase.getInstance().getReference().child("hangouts").setValue(hangouts);
                FirebaseDatabase.getInstance().getReference().child("hangouts").removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                FirebaseDatabase.getInstance().getReference().child("hangouts").removeEventListener(this);
            }
        });
    }


}
