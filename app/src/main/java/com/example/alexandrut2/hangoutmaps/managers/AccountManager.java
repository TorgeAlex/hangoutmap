package com.example.alexandrut2.hangoutmaps.managers;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.example.alexandrut2.hangoutmaps.utils.ApplicationClass;
import com.example.alexandrut2.hangoutmaps.R;
import com.example.alexandrut2.hangoutmaps.mvp.model.FriendsList;
import com.example.alexandrut2.hangoutmaps.mvp.model.LocationModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.PreferencesModel;
import com.example.alexandrut2.hangoutmaps.mvp.model.User;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by alexandrut2 on 3/4/17.
 */

public class AccountManager {

    public static final String USERS_DB_PATH = ApplicationClass.getContext().getResources().getString(R.string.users_database_url);

    public static final String LOCATIONS_DB_PATH = ApplicationClass.getContext().getResources().getString(R.string.users_location_database_url);

    public static final String FRIENDS_DB_PATH = ApplicationClass.getContext().getResources().getString(R.string.friends_database_url);

    public static final String PREFERENCES_DB_PATH = ApplicationClass.getContext().getResources().getString(R.string.preferences_database_url);

    private static final String TAG = "AlexT";
    /**
     * static instance for AccountManager
     */
    private static AccountManager instance;

    /**
     * Instance for FirebaseAuth
     */
    private FirebaseAuth firebaseAuth;

    /**
     * Listener for register states
     */
    private OnRegisterResponseListener OnRegisterResponseListener;

    /**
     * Listener for login states
     */
    private OnLoginResponseListener OnLoginResponseListener;

    /**
     * Listener for google sign in states
     */
    private OnGoogleSignInResponseListener OnGoogleSignInResponseListener;

    /**
     * Listener for user attributes
     */
    private OnUserAttributesChangedListener userAttributesChangedListener;

    /**
     * Listener for friends
     */
    private OnFriendsChangedListener friendsChangedListener;

    /**
     * User
     */
    private User currentUser;

    /**
     * Friends
     */
    private List<User> friends;

    /**
     * FriendList, only id's
     */
    private FriendsList friendsList;

    /**
     * Current Firebase User
     */
    private FirebaseUser currentFirebaseUser;

    /**
     * Main database reference
     */
    private DatabaseReference usersDatabaseReference;

    /**
     * Listener for user changes
     */
    private ValueEventListener userDbListener;

    /**
     * Listener for friends changes
     */
    private ValueEventListener friendsDbListener;

    /**
     * Listener for preferences changes
     */
    private ValueEventListener preferencesDbListener;

    private LocationModel currentLocationModel;

    private PreferencesModel currentPreferencesModel;



    String usersPath;


    private AccountManager() {
        usersPath= ApplicationClass.getContext().getResources().getString(R.string.users_database_url);
        firebaseAuth = FirebaseAuth.getInstance();
        usersDatabaseReference = FirebaseDatabase.getInstance().getReference().child(usersPath);
        userDbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                Log.e("AlexT","onDataChange");
                User user = dataSnapshot.getValue(User.class);
                Log.e("AlexT","User isNull:"+(user==null));
                if(user != null){
                    currentUser = user;
                } else {
                    currentUser = new User(currentFirebaseUser.getDisplayName(), currentFirebaseUser.getEmail(), currentFirebaseUser.getUid());
                    Log.e("AlexT","FIREBASE USER NAME:"+currentFirebaseUser.getDisplayName());
                    updateCurrentUser();
                }
                if(userAttributesChangedListener != null){
                    userAttributesChangedListener.userAttributesChanged(currentUser);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "load User:onCancelled", databaseError.toException());
                // ...
            }
        };

        friendsDbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FriendsList fl = dataSnapshot.getValue(FriendsList.class);
                if(fl != null){
                    friendsList = fl;
                    FirebaseDatabase.getInstance().getReference().child(USERS_DB_PATH).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(friends!=null && !friends.isEmpty()) {
                                friends.clear();
                            }else {
                                friends = new LinkedList<>();
                            }
                            Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                            for(DataSnapshot child: children){
                                User friend = child.getValue(User.class);
                                if(friend != null && friendsList.getFriends().contains(friend.getUid())){
                                    friends.add(friend);
                                }
                            }
                            Log.e("ALexT","update friends size="+fl.getFriends().size());
                            if(friendsChangedListener != null){
                                friendsChangedListener.friendsChanged(friends);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, "Failed to get friends");
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        preferencesDbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                PreferencesModel pm = dataSnapshot.getValue(PreferencesModel.class);
                if(pm != null){
                    currentPreferencesModel = pm;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


    }

    /**
     * @return the instance of AccountManager
     */
    public static AccountManager getInstance(){
        if(instance == null){
            instance = new AccountManager();
        }
        return instance;
    }

    //////////////////////////////  Login & sign in methods  //////////////////////////////

    /**
     * Registers an user in Firebase database
     * @param username
     * @param email
     * @param password
     */
    public void registerUser(final String username, final String email, String password){
        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    OnRegisterResponseListener.onCreateUserFailed(task.getException().getMessage());
                } else {
                    currentFirebaseUser = task.getResult().getUser();
                    if(currentFirebaseUser!=null) {
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(username).build();
                        currentFirebaseUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Log.e("AlexT","onComplete");
                                currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                //updateCurrentUser();
                                setListenerForUserUpdates();
                            }
                        });
                        OnRegisterResponseListener.onCreateUserSuccessfully();
                    }
                }
            }
        });
    }

    /**
     * Logs in an user in Firebase database
     * @param email
     * @param password
     */
    public void login(String email, String password){
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(!task.isSuccessful()){
                    OnLoginResponseListener.onLogInFailed(task.getException().getMessage());
                    return;
                }else{
                    currentFirebaseUser = task.getResult().getUser();
                    setListenerForUserUpdates();
                    //updateCurrentUser();
                    OnLoginResponseListener.onLogInSuccessfully();
                }
            }
        });
    }

    /**
     * Signs in with google account
     * @param acct
     */
    public void signInhWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            if(OnGoogleSignInResponseListener != null) {
                                OnGoogleSignInResponseListener.onGoogleSignInFailed(task.getException().getMessage());
                            }
                            return;
                        }
                        currentFirebaseUser = task.getResult().getUser();
                        if(TextUtils.isEmpty(currentFirebaseUser.getDisplayName())) {
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(currentFirebaseUser.getEmail()).build();
                            currentFirebaseUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                                    //updateCurrentUser();
                                    setListenerForUserUpdates();
                                }
                            });
                        }else{
                            setListenerForUserUpdates();
                        }
                        if(OnGoogleSignInResponseListener != null) {
                            OnGoogleSignInResponseListener.onGoogleSignInSuccessfully();
                        }
                    }
                });
    }


    public boolean isUserAlreadyLoggedIn(){
        if(firebaseAuth.getCurrentUser() != null){
            return true;
        }
        return false;
    }

    public void handleUserAlreadyLoggedIn(){
        currentFirebaseUser = firebaseAuth.getCurrentUser();
        setListenerForUserUpdates();
    }


    /**
     * Signs out the user
     */
    public void signOut(){
        firebaseAuth.signOut();
    }




    /////////////////////// UPDATE METHODS ///////////////////////////

    /**
     * Updates the user name
     * @param name
     */
    public void updateUserName(String name){
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
        currentFirebaseUser.updateProfile(profileUpdates);
        if(currentUser == null) {
            currentUser = new User();
            currentUser.setName(name);
            currentUser.setEmail(currentFirebaseUser.getEmail());
        }else{
            currentUser.setName(name);
        }
        updateCurrentUser();
    }


    /**
     * Updates the user location
     * @param locationModel
     */
    public void updateUserLocation(LocationModel locationModel){
        String uid = getCurrentFirebaseUser().getUid();
        currentLocationModel = locationModel;
        /*String email = getCurrentFirebaseUser().getEmail();
        String encodedEmail = AppUtils.encode(email);
        Log.e("AlexT","Update user location with encoded email : "+encodedEmail);*/
        getDatabaseReference().child(LOCATIONS_DB_PATH).child(uid).setValue(locationModel);
    }

    /**
     * Updates the user preferences
     * @param preferencesModel
     */
    public void updatePreferences(PreferencesModel preferencesModel){
        String uid = getCurrentFirebaseUser().getUid();
        this.currentPreferencesModel = preferencesModel;
        getDatabaseReference().child(PREFERENCES_DB_PATH).child(uid).setValue(preferencesModel);
    }


    /**
     * Add a friend to a user
     */
    public void addFriend(final User u){
        if (friendsList != null) {
            if (friendsList.containsFriend(u)) {
                return;
            }
        }
        getDatabaseReference().child(FRIENDS_DB_PATH).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                FriendsList fl1 = null;
                FriendsList fl2 = null;

                for (DataSnapshot friendsList:children) {
                        FriendsList currentFl = friendsList.getValue(FriendsList.class);
                        if(currentFl.getEmail().equals(currentUser.getEmail())){
                            fl1 = currentFl;
                            fl1.addFriend(u.getUid());
                        } else if(currentFl.getEmail().equals(u.getEmail())){
                            fl2 = currentFl;
                            fl2.addFriend(currentUser.getUid());
                        }
                }
                Log.e("AlexT","Finished one iterating");
                if(fl1 == null){
                    fl1 = new FriendsList();
                    fl1.setEmail(currentUser.getEmail());
                    fl1.addFriend(u.getUid());
                }
                getDatabaseReference().child(FRIENDS_DB_PATH).child(currentUser.getUid()).setValue(fl1);

                if(fl2 == null){
                    fl2 = new FriendsList();
                    fl2.setEmail(u.getEmail());
                    fl2.addFriend(currentUser.getUid());
                }
                getDatabaseReference().child(FRIENDS_DB_PATH).child(u.getUid()).setValue(fl2);

                getDatabaseReference().child(FRIENDS_DB_PATH).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Updates the user image
     * @param image
     */
    public void updateUserImage(String image){
        if(currentUser == null) {
            currentUser = new User();
            currentUser.setName(currentFirebaseUser.getDisplayName());
            currentUser.setEmail(currentFirebaseUser.getEmail());
        }
        currentUser.setImage(image);
        updateCurrentUser();
    }



    //////////////////////////////   SETTERS FOR LISTENERS  //////////////////////////////

    public void setOnRegisterResponseListener(OnRegisterResponseListener OnRegisterResponseListener){
        this.OnRegisterResponseListener = OnRegisterResponseListener;
    }

    public void setOnLoginResponseListener(OnLoginResponseListener OnLoginResponseListener) {
        this.OnLoginResponseListener = OnLoginResponseListener;
    }

    public void setOnGoogleSignInResponseListener(OnGoogleSignInResponseListener OnGoogleSignInResponseListener) {
        this.OnGoogleSignInResponseListener = OnGoogleSignInResponseListener;
    }


    public void setOnUserAttributesChangedListener(OnUserAttributesChangedListener listener) {
        this.userAttributesChangedListener = listener;
    }

    public void setFriendsChangedListener(OnFriendsChangedListener friendsChangedListener) {
        this.friendsChangedListener = friendsChangedListener;
    }

    private void setListenerForUserUpdates(){
        String uid = getCurrentFirebaseUser().getUid();
        Log.e("AlexT","AccountManager: setListenerForUserUpdates: uid="+uid);
        getDatabaseReference().child(PREFERENCES_DB_PATH).child(uid).addValueEventListener(preferencesDbListener);
        getDatabaseReference().child(USERS_DB_PATH).child(uid).addValueEventListener(userDbListener);
        getDatabaseReference().child(FRIENDS_DB_PATH).child(uid).addValueEventListener(friendsDbListener);
    }


    /**
     * Listener for register operation
     */
    public interface OnRegisterResponseListener {

        void onCreateUserSuccessfully();

        void onCreateUserFailed(String status);

    }

    /**
     * Listener for google sign in
     */
    public interface OnGoogleSignInResponseListener {

        void onGoogleSignInSuccessfully();

        void onGoogleSignInFailed(String status);

    }


    /**
     * Listener for login operation
     */
    public interface OnLoginResponseListener {

        void onLogInSuccessfully();

        void onLogInFailed(String status);

    }

    /**
     * Listener for user attributes changed
     */
    public interface OnUserAttributesChangedListener {
        void userAttributesChanged(User user);
    }

    /**
     * Listener for users friends change
     */
    public interface OnFriendsChangedListener{
        void friendsChanged(List<User> friends);
    }


    //////////////////////////////   PRIVATE METHODS  //////////////////////////////

    private void updateCurrentUser(){
        Log.e("AlexT","Update current user");
        if(currentUser == null){
            currentUser = new User();
            currentUser.setName(currentFirebaseUser.getDisplayName());
            currentUser.setEmail(currentFirebaseUser.getEmail());
            currentUser.setUid(currentFirebaseUser.getUid());
        }
        String uid = getCurrentFirebaseUser().getUid();
        getDatabaseReference().child(USERS_DB_PATH).child(uid).setValue(currentUser);
}

    //////////////////////////////   GETTERS  //////////////////////////////


    public FirebaseUser getCurrentFirebaseUser() {
        return currentFirebaseUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public DatabaseReference getUsersDatabaseReference() {
        return usersDatabaseReference;
    }

    public DatabaseReference getDatabaseReference(){
        return FirebaseDatabase.getInstance().getReference();
    }

    public PreferencesModel getCurrentPreferencesModel() {
        if(currentPreferencesModel == null){
            currentPreferencesModel = new PreferencesModel();
            currentPreferencesModel.addPreference("restaurant");
        }
        return currentPreferencesModel;
    }

    public List<User> getFriends(){
        return friends;
    }

    public FriendsList getFriendsList(){
        return friendsList;
    }

    public LocationModel getCurrentLocationModel(){
        return currentLocationModel;
    }
}
